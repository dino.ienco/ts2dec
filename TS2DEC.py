




"""
Keras implementation for TS2DEC (Triplet-driven Semi-Supervised Deep Embedded Clustering) algorithm:

        Junyuan Xie, Ross Girshick, and Ali Farhadi. Unsupervised deep embedding for clustering analysis. ICML 2016.

Usage:
    use `python TS2DEC.py -h` for help.

Author:
	Dino Ienco. 2019.2.26
	
	

The current code is based on the Keras implementation for Deep Embedded Clustering (DEC) algorithm:
			Junyuan Xie, Ross Girshick, and Ali Farhadi. Unsupervised deep embedding for clustering analysis. ICML 2016.
			Author: Xifeng Guo. 2017.1.30
			Availability: https://github.com/XifengGuo/DEC-keras

"""

from time import time
import numpy as np
from sklearn.utils import shuffle
import keras.backend as K
from keras.engine.topology import Layer, InputSpec
from keras.layers import Dense, Input, Lambda, Subtract, Activation
from keras.models import Model
from keras.optimizers import SGD, Adam, RMSprop
from keras import callbacks
from keras.initializers import VarianceScaling
from sklearn.cluster import KMeans
import random
import metrics
import argparse
import os
from keras import losses

###################################
# TensorFlow wizardry
#config = tf.ConfigProto()
 
# Don't pre-allocate memory; allocate as-needed
#config.gpu_options.allow_growth = True
 
# Only allow a total of half the GPU memory to be allocated
#config.gpu_options.per_process_gpu_memory_fraction = 0.5
 
# Create a session with the above options specified.
#k.tensorflow_backend.set_session(tf.Session(config=config))
###################################


def sample_constraints(f_data, s_data, t_data, triplets2check, perc):
	hash_ = {}
	nrow, _ = triplets2check.shape
	# for each pair of anchor, positive enumeartes the list of positions concerning this tuple despite the fact that we have called the shuffle method before
	for i in range(nrow):
		a, p, n = triplets2check[i]
		if (a,p) not in hash_:
			hash_[a,p] = []
		hash_[a,p].append(i)
	
	t_f_data = []
	t_s_data = []
	t_t_data = []
	t_t_link = []
	
	# sample the perc number of negative examples for each pair (anchor, positive)
	for k in hash_.keys():
		a, p = k
		for pos in hash_[k]:
			if random.random() < perc:
				t_f_data.append( f_data[pos] )
				t_s_data.append( s_data[pos] )
				t_t_data.append( t_data[pos] )
				t_t_link.append( 1 )
				
	return np.array(t_f_data), np.array(t_s_data), np.array(t_t_data), np.array(t_t_link)

def extractTriplet(data, n_labeled_samples, id_n_labeled_samples, directory):
	lab_examples = np.load(directory+"/constraints/"+str(id_n_labeled_samples)+"_"+str(n_labeled_samples)+".npy")
	#lab_examples = num_labeled_examples X 2
	# lab_examples[:,0] = example ID
	# lab_examples[:,1] = class ID exploited to produce the triplet constraint
	
	nrow = len(lab_examples)
	
	f_data = []
	s_data = []
	t_data = []
	triplets = []
	for i in range(nrow):
		el1 = int( lab_examples[i][0] )
		for j in range(i+1,nrow):
			el2 = int( lab_examples[j][0])
			for k in range(nrow):
				el3 = int( lab_examples[k][0] )
				if lab_examples[i][1] == lab_examples[j][1] and lab_examples[i][1] != lab_examples[k][1]:
					if el1 != el2:
						f_data.append(data[el1])
						s_data.append(data[el2])
						t_data.append(data[el3])
						triplets.append( [el1,el2,el3] )
						
	
	return np.array(f_data), np.array(s_data), np.array(t_data), np.array(triplets)


def triplet_loss(y_true, y_pred):
    '''Triplet loss modeled via Contrastive loss from Hadsell-et-al.'06
    http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf
    '''
    margin = 1.
    loss = K.maximum(y_pred + margin, 0)
    return K.mean(loss) + (0. * y_true)

def squared_euclidean_distance(vects):
    x, y = vects
    sum_square = K.sum(K.square(x - y), axis=1, keepdims=True)
    return sum_square

def eucl_dist_output_shape(shapes):
    shape1, shape2 = shapes
    return (shape1[0], 1)

def triplet_autoencoder(ae, encoder, input_shape):
	"""
	Fully connected triplet auto-encoder model with shared weights, symmetric
	Arguments:
        ae: 			autoencoder model
		encoder:		the encoder model (to retrieve embedding representation of the data)
		input_shape:	the shape of the input tensor	
    return:
        Model of triplet autoencoder with shared weights
	"""
	input_a = Input(shape=(input_shape,))
	input_b = Input(shape=(input_shape,))
	input_c = Input(shape=(input_shape,))
	enc1 = encoder(input_a)
	enc2 = encoder(input_b)
	enc3 = encoder(input_c)
    
	enc1 = Lambda(lambda  x: K.l2_normalize(x,axis=1))(enc1)
	enc2 = Lambda(lambda  x: K.l2_normalize(x,axis=1))(enc2)
	enc3 = Lambda(lambda  x: K.l2_normalize(x,axis=1))(enc3)

	distance1 = Lambda(squared_euclidean_distance,
                 output_shape=eucl_dist_output_shape)([enc1, enc2])

	distance2 = Lambda(squared_euclidean_distance,
                 output_shape=eucl_dist_output_shape)([enc1, enc3])

	distance3 = Lambda(squared_euclidean_distance,
                 output_shape=eucl_dist_output_shape)([enc2, enc3])


	diff1 = Subtract()([distance1, distance2])
	diff2 = Subtract()([distance1, distance3])
	
	model = Model(inputs=[input_a,input_b,input_c],outputs=[ae(input_a), ae(input_b), ae(input_c), diff1, diff2], name="tripletNet")
	return model

def autoencoder(dims, act='relu', init='glorot_uniform'):
    """
    Fully connected auto-encoder model, symmetric.
    Arguments:
        dims: list of number of units in each layer of encoder. dims[0] is input dim, dims[-1] is units in hidden layer.
            The decoder is symmetric with encoder. So number of layers of the auto-encoder is 2*len(dims)-1
        act: activation, not applied to Input, Hidden and Output layers
    return:
        (ae_model, encoder_model), Model of autoencoder and model of encoder
    """
    n_stacks = len(dims) - 1
    # input
    x = Input(shape=(dims[0],), name='input')
    h = x

    # internal layers in encoder
    for i in range(n_stacks-1):
        h = Dense(dims[i + 1], activation=act, kernel_initializer=init, name='encoder_%d' % i)(h)

    # hidden layer
    h = Dense(dims[-1], kernel_initializer=init, name='encoder_%d' % (n_stacks - 1))(h)  # hidden layer, features are extracted from here
 
    y = h
    # internal layers in decoder
    for i in range(n_stacks-1, 0, -1):
        y = Dense(dims[i], activation=act, kernel_initializer=init, name='decoder_%d' % i)(y)

    # output
    y = Dense(dims[0], kernel_initializer=init, activation=None,name='decoder_0')(y)

    return Model(inputs=x, outputs=y, name='AE'), Model(inputs=x, outputs=h, name='encoder')


class ClusteringLayer(Layer):
    """
    Clustering layer converts input sample (feature) to soft label, i.e. a vector that represents the probability of the
    sample belonging to each cluster. The probability is calculated with student's t-distribution.

    # Example
    ```
        model.add(ClusteringLayer(n_clusters=10))
    ```
    # Arguments
        n_clusters: number of clusters.
        weights: list of Numpy array with shape `(n_clusters, n_features)` witch represents the initial cluster centers.
        alpha: parameter in Student's t-distribution. Default to 1.0.
    # Input shape
        2D tensor with shape: `(n_samples, n_features)`.
    # Output shape
        2D tensor with shape: `(n_samples, n_clusters)`.
    """

    def __init__(self, n_clusters, weights=None, alpha=1.0, **kwargs):
        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)
        super(ClusteringLayer, self).__init__(**kwargs)
        self.n_clusters = n_clusters
        self.alpha = alpha
        self.initial_weights = weights
        self.input_spec = InputSpec(ndim=2)

    def build(self, input_shape):
        assert len(input_shape) == 2
        input_dim = input_shape[1]
        self.input_spec = InputSpec(dtype=K.floatx(), shape=(None, input_dim))
        self.clusters = self.add_weight((self.n_clusters, input_dim), initializer='glorot_uniform', name='clusters')
        if self.initial_weights is not None:
            self.set_weights(self.initial_weights)
            del self.initial_weights
        self.built = True

    def call(self, inputs, **kwargs):
        """ student t-distribution, as same as used in t-SNE algorithm.
                 q_ij = 1/(1+dist(x_i, u_j)^2), then normalize it.
        Arguments:
            inputs: the variable containing data, shape=(n_samples, n_features)
        Return:
            q: student's t-distribution, or soft labels for each sample. shape=(n_samples, n_clusters)
        """
        q = 1.0 / (1.0 + (K.sum(K.square(K.expand_dims(inputs, axis=1) - self.clusters), axis=2) / self.alpha))
        q **= (self.alpha + 1.0) / 2.0
        q = K.transpose(K.transpose(q) / K.sum(q, axis=1))
        return q

    def compute_output_shape(self, input_shape):
        assert input_shape and len(input_shape) == 2
        return input_shape[0], self.n_clusters

    def get_config(self):
        config = {'n_clusters': self.n_clusters}
        base_config = super(ClusteringLayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class TS2DEC(object):
    def __init__(self,
                 dims,
                 n_clusters=10,
                 alpha=1.0,
                 init='glorot_uniform'):

		super(TS2DEC, self).__init__()

		self.dims = dims
		self.input_dim = dims[0]
		self.n_stacks = len(self.dims) - 1
		
		self.n_clusters = n_clusters
		self.alpha = alpha
		self.autoencoder, self.encoder = autoencoder(self.dims, init=init)
		self.triplet = triplet_autoencoder(self.autoencoder, self.encoder, self.dims[0])
		
		# prepare TS2DEC model
		clustering_layer = ClusteringLayer(self.n_clusters, name='clustering')(self.encoder.get_output_at(0))        
		self.model = Model(inputs=self.encoder.get_input_at(0), outputs=clustering_layer)
		self.feature_extractor = Model(self.model.get_input_at(0), self.model.get_layer('encoder_%d' % (len(self.dims)-2)).output)
	
	
	
    def pretrain(self, x, t_f_data, t_s_data, t_t_data, t_t_link, l_directory, constr_sample, n_samples, y=None, optimizer='adam', epochs=200, batch_size=256, save_dir='results/temp'):
		print('...Pretraining...')
		
		csv_logger = callbacks.CSVLogger(save_dir + '/pretrain_log.csv')
		cb = [csv_logger]
		if y is not None:
			class PrintACC(callbacks.Callback):
				def __init__(self, x, y):
					self.x = x
					self.y = y
					super(PrintACC, self).__init__()

				def on_epoch_end(self, epoch, logs=None):
					if int(epochs/10) != 0 and epoch % int(epochs/10) != 0:
						return
					feature_model = Model(self.model.get_input_at(0),
                                          self.model.get_layer(
                                              'encoder_%d' % (int(len(self.model.layers) / 2) - 1)).output)
					features = feature_model.predict(self.x)
					print "features"
					print features.shape
					#features = self.feature_extractor.predict(self.x)
					km = KMeans(n_clusters=len(np.unique(self.y)), n_init=20, n_jobs=4)
					y_pred = km.fit_predict(features)
					# print()
					print(' '*8 + '|==>  acc: %.4f,  nmi: %.4f  <==|'
                          % (metrics.acc(self.y, y_pred), metrics.nmi(self.y, y_pred)))

			cb.append(PrintACC(x, y))

		# begin pretraining
		t0 = time()
		sample_card = 50000
		
		print "n constraints selected %d over %d" % (len(t_f_data), len(triplets2check))
		print "n of epochs %d" %epochs
		for i in range(epochs):
			print "epoch %d" % i
			x, y = shuffle(x, y, random_state=0)
			
			self.autoencoder.fit(x, x, batch_size=batch_size, epochs=1, shuffle = False, verbose=0)

			t_f_data, t_s_data, t_t_data, t_t_link = shuffle( t_f_data,t_s_data,t_t_data, t_t_link )
			self.triplet.fit([t_f_data,t_s_data,t_t_data], [t_f_data,t_s_data,t_t_data, t_t_link, t_t_link], batch_size=64, epochs=1, shuffle = True, verbose=1)
			print('Pretraining time: %ds' % round(time() - t0))
			
			features = self.feature_extractor.predict(x)
			km = KMeans(n_clusters=self.n_clusters, n_init=30, n_jobs=4)
			y_clusters = km.fit_predict(features)
			
			print(' '*8 + '|==>  acc: %.4f,  nmi: %.4f  <==|'
                  % (metrics.acc(y, y_clusters), metrics.nmi(y, y_clusters)))
			
			
			del y_clusters
			del features
        
		#emb_features = self.encoder.fit(x)
		self.autoencoder.save_weights(save_dir + '/ae_weights.h5')
		print('Pretrained weights are saved to %s/ae_weights.h5' % save_dir)
		self.pretrained = True

    def load_weights(self, weights):  # load weights of DEC model
        self.model.load_weights(weights)

    def extract_features(self, x):
        return self.encoder.predict(x)

    def predict(self, x):  # predict cluster labels using the output of clustering layer
        q = self.model.predict(x, verbose=0)
        return q.argmax(1)

    @staticmethod
    def target_distribution(q):
        weight = q ** 2 / q.sum(0)
        return (weight.T / weight.sum(1)).T

    def compile(self, optimizer='Adam', loss='kld'):
        self.model.compile(optimizer=optimizer, loss=loss)

    def fit(self, x, t_f_data, t_s_data, t_t_data, t_t_link, y=None, maxiter=2e4, batch_size=256, tol=1e-3,
            update_interval=140, save_dir='./results/temp'):

		print('Update interval', update_interval)
		save_interval = int(x.shape[0] / batch_size) * 5  # 5 epochs
		print('Save interval', save_interval)

		# Step 1: initialize cluster centers using k-means
		t1 = time()
		print('Initializing cluster centers with k-means.')
        
		kmeans = KMeans(n_clusters=self.n_clusters, n_init=20)
		y_pred = kmeans.fit_predict(self.encoder.predict(x))
		y_pred_last = np.copy(y_pred)
		self.model.get_layer(name='clustering').set_weights([kmeans.cluster_centers_])

        # Step 2: deep clustering
        # logging file
		import csv
		logfile = open(save_dir + '/ts2dec_log.csv', 'w')
		logwriter = csv.DictWriter(logfile, fieldnames=['iter', 'acc', 'nmi', 'ari', 'loss'])
		logwriter.writeheader()

		loss = 0
		index = 0
		index_array = np.arange(x.shape[0])
		for ite in range(int(maxiter)):
			if ite % update_interval == 0:				
				q = self.model.predict(x, verbose=0)
				p = self.target_distribution(q)  # update the auxiliary target distribution p

				# evaluate the clustering performance
				y_pred = q.argmax(1)
				if y is not None:
					acc = np.round(metrics.acc(y, y_pred), 5)
					nmi = np.round(metrics.nmi(y, y_pred), 5)
					ari = np.round(metrics.ari(y, y_pred), 5)
					loss = np.round(loss, 5)
					logdict = dict(iter=ite, acc=acc, nmi=nmi, ari=ari, loss=loss)
					logwriter.writerow(logdict)
					print('Iter %d: acc = %.5f, nmi = %.5f, ari = %.5f' % (ite, acc, nmi, ari), ' ; loss=', loss)

				# check stop criterion
				delta_label = np.sum(y_pred != y_pred_last).astype(np.float32) / y_pred.shape[0]
				y_pred_last = np.copy(y_pred)
				if ite > 0 and delta_label < tol:
					print('delta_label ', delta_label, '< tol ', tol)
					print('Reached tolerance threshold. Stopping training.')
					logfile.close()
					break

			idx = index_array[index * batch_size: min((index+1) * batch_size, x.shape[0])]
			loss = self.model.train_on_batch(x=x[idx], y=p[idx])
			index = index + 1 if (index + 1) * batch_size <= x.shape[0] else 0
			
			if ite != 0 and index == 0:
				t_f_data,t_s_data,t_t_data, t_t_link = shuffle( t_f_data,t_s_data,t_t_data, t_t_link )
				self.triplet.fit([t_f_data,t_s_data,t_t_data], [t_f_data,t_s_data,t_t_data, t_t_link, t_t_link], batch_size=64, epochs=1, shuffle = True, verbose=1)
			
			# save intermediate model
			if ite % save_interval == 0:
				print('saving model to:', save_dir + '/TS2DEC_model_' + str(ite) + '.h5')
				self.model.save_weights(save_dir + '/TS2DEC_model_' + str(ite) + '.h5')

			ite += 1

		# save the trained model
		logfile.close()
		print('saving model to:', save_dir + '/TS2DEC_model_final.h5')
		self.model.save_weights(save_dir + '/TS2DEC_model_final.h5')

		return y_pred


if __name__ == "__main__":
	# setting the hyper parameters
	###################	###################	###################	###################
	import tensorflow as tf
	from keras.backend.tensorflow_backend import set_session
	config = tf.ConfigProto()
	config.gpu_options.per_process_gpu_memory_fraction = 0.5
	set_session(tf.Session(config=config))
	###################	###################	###################	###################
	

	parser = argparse.ArgumentParser(description='train',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('--dataset')
	parser.add_argument('--batch_size', default=256, type=int)
	parser.add_argument('--maxiter', default=2e4, type=int)
	parser.add_argument('--pretrain_epochs', default=None, type=int)
	parser.add_argument('--update_interval', default=None, type=int)
	parser.add_argument('--tol', default=0.001, type=float)
	parser.add_argument('--ae_weights', default=None)
	parser.add_argument('--save_dir', default='results')
	parser.add_argument('--run_id', type=int)
	parser.add_argument('--n_samples', type=int)
	parser.add_argument('--constr_sample', type=int)
	parser.add_argument('--lambda_val', default= 0.001, type=float)
	args = parser.parse_args()
	print(args)
	if not os.path.exists(args.save_dir):
		os.makedirs(args.save_dir)

	lambda_val = args.lambda_val

	# load dataset
	#from datasets import load_data
	datName = args.dataset
	x = np.load(datName+"/data.npy")
	y = np.load(datName+"/class.npy")
	constr_sample = args.constr_sample
	n_samples = args.n_samples
	run_id = args.run_id
	n_clusters = len(np.unique(y))

	init = 'glorot_uniform'
	pretrain_optimizer = 'adam'#Adam(0.0001)
	#pretrain_optimizer = 'rmsprop'
	# setting parameters
	update_interval = 30
	pretrain_epochs = 50 #50
	if args.update_interval is not None:
		update_interval = args.update_interval
	if args.pretrain_epochs is not None:
		pretrain_epochs = args.pretrain_epochs

	# prepare the DEC model
	ts2dec = TS2DEC(dims=[x.shape[-1], 500, 500, 2000, 10], n_clusters=n_clusters, init=init)
	
	
	f_data, s_data, t_data, triplets2check = extractTriplet(x, n_samples, constr_sample, datName)
	f_data, s_data, t_data, triplets2check = shuffle(f_data, s_data, t_data, triplets2check)
	
	t_f_data, t_s_data, t_t_data, t_t_link = sample_constraints(f_data, s_data, t_data, triplets2check, .3)
	
	ts2dec.autoencoder.compile(optimizer='adam', loss='mse')
	ts2dec.triplet.compile(optimizer='adam', loss=['mse','mse','mse',triplet_loss,triplet_loss], loss_weights=[1.,1.,1.,lambda_val,lambda_val])

	if args.ae_weights is None:
		ts2dec.pretrain(x=x, t_f_data=t_f_data, t_s_data=t_s_data, t_t_data=t_t_data, t_t_link=t_t_link, l_directory=datName, constr_sample=constr_sample, n_samples=n_samples, y=y, optimizer=pretrain_optimizer,
                     epochs=pretrain_epochs, batch_size=args.batch_size,
                     save_dir=args.save_dir)
	else:
		ts2dec.autoencoder.load_weights(args.ae_weights)

	ts2dec.model.summary()
	t0 = time()
	ts2dec.model.compile(optimizer=SGD(0.01, 0.9), loss='kld')
	ts2dec.triplet.compile(optimizer='adam', loss=['mse','mse','mse',triplet_loss,triplet_loss], loss_weights=[0.,0.,0.,lambda_val,lambda_val])
		
	y_pred = ts2dec.fit(x, t_f_data, t_s_data, t_t_data, t_t_link, y=y, tol=args.tol, maxiter=args.maxiter, batch_size=args.batch_size,
                     update_interval=update_interval, save_dir=args.save_dir)
    
    
    
    
	print('acc:', metrics.acc(y, y_pred))
	print('clustering time: ', (time() - t0))

	folder_path = args.dataset+"/TS2DEC"
	if not os.path.exists(folder_path):
		os.makedirs(folder_path)
	
	out_file_name = folder_path+"/"+str(constr_sample)+"_"+str(n_samples)+"_"+str(run_id)+".npy"
	np.save(out_file_name, y_pred)
	
	features = ts2dec.extract_features(x)
	out_file_name = folder_path+"/features_"+str(constr_sample)+"_"+str(n_samples)+"_"+str(run_id)+".npy"
	np.save(out_file_name, features)
	
	
	
	
